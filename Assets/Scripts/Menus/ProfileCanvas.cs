﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TFGFran.Networking;
using TFGFran.Player;

public class ProfileCanvas : MonoBehaviour {

    public Text PlayerName;
    public Slider ExpBar;
    public Text LevelText;
    public Text ExpNeedText;

	// Use this for initialization
	void Start () {
        UpdateProfile();
    }
	
	public void UpdateProfile() {
        PlayerProfile pp = ConnectionManager.Instance.PlayerProfile;
        PlayerName.text = pp.Name;
        ExpBar.maxValue = pp.ExpToLevelUp;
        ExpBar.value = pp.CurrentExp;
        LevelText.text = pp.Level.ToString();
        ExpNeedText.text = string.Format("{0}/{1}\n{2} more.", pp.CurrentExp, pp.ExpToLevelUp, pp.RemainingExpToLevelUp);
    }
}
