﻿using UnityEngine;
using TFGFran.Networking;

public class DebugProfile : MonoBehaviour {

    private ProfileCanvas pc;

    void Awake() {
        pc = FindObjectOfType<ProfileCanvas>();
    }

    public void AddExperience(int value) {
        ConnectionManager.Instance.PlayerProfile.AddExp(value, 1);
        Debug.Log(ConnectionManager.Instance.PlayerProfile.ToString());

        pc.UpdateProfile();
    }
}
