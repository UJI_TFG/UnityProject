﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using TFGFran.Networking;
using TFGFran.MatchMaking;

public class CloseHook : MonoBehaviour {

    void OnApplicationQuit () {
        ConnectionManager.Instance.Disconnect();

        if(NetworkClient.active) {
            MatchMaker matchMaker = FindObjectOfType<MatchMaker>();

            matchMaker.DropConnection();
        }

        //Application.CancelQuit();
    }
}
