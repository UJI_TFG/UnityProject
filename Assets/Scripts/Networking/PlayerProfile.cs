﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using TFGCommonLib.Networking;
using TFGFran.Networking;

namespace TFGFran.Player {

    public class PlayerProfile {
        private string name;
        public string Name {
            get {
                return name;
            }
        }
        private int userID;
        private int currentExp;

        public int Level {
            get {
                return (int)(Math.Sqrt(100 * (2 * currentExp + 25)) + 50) / 100;
            }
        }
        public int ExpToLevelUp {
            get {
                return ((int)(Math.Pow(Level + 1, 2) + (Level + 1)) / 2 * 100 - ((Level + 1) * 100)) - LevelOffset;
            }
        }
        public int LevelOffset {
            get {
                return (int)(Math.Pow(Level, 2) + (Level + 1)) / 2 * 100 - ((Level) * 100);
            }
        }
        public int RemainingExpToLevelUp {
            get {
                return ExpToLevelUp - currentExp + LevelOffset;
            }
        }
        public int CurrentExp {
            get {
                return currentExp - LevelOffset;
            }
        }

        public int ID {
            get {
                return userID;
            }
        }

        public PlayerProfile(string name, int userID, int currentExp) {
            this.name = name;
            this.userID = userID;
            this.currentExp = currentExp;
        }

        public override string ToString() {
            TextInfo myTI = new CultureInfo("es-ES", false).TextInfo;
            StringBuilder sb = new StringBuilder(myTI.ToTitleCase(name));
            sb.AppendFormat(" [Level: {0} || {1} more || {2}/{3}]", Level, RemainingExpToLevelUp, CurrentExp, ExpToLevelUp);

            return sb.ToString();
        }

        public void AddExp(int value, int rate) {
            currentExp += value * rate;
            if (userID != -1)
                ConnectionManager.Instance.Send(OpCode.UPDATE_USER, currentExp.ToString());
        }
    }
}