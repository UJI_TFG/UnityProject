﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Net.Sockets;
using UnityEngine;
using System.Text;
using TFGCommonLib.Networking;
using System.Collections;
using System.Net;
using TFGFran.Player;

namespace TFGFran.Networking {
    class ConnectionManager : ConnectionProcessor {

        private static ConnectionManager instance;
        public static ConnectionManager Instance {
            get {
                if (instance == null)
                    instance = new ConnectionManager();
                return instance;
            }
        }

        public string serverIP = "192.168.1.10";
        public int port = 1994;

        private List<string> messagesReceived = new List<string>();

        //private TcpClient clientSocket;
        /*public TcpClient ClientSocket {
            get {
                return clientSocket;
            }
        }*/

        private Socket clientSocket;
        public Socket ClientSocket {
            get {
                return clientSocket;
            }
        }

        private Thread receiver;

        private PlayerProfile playerProfile;
        public PlayerProfile PlayerProfile {
            get {
                if(playerProfile == null) {
                    playerProfile = new PlayerProfile("Editor", -1, 42);
                }

                return playerProfile;
            }
        }

        /// <summary>
        /// Connects the ClientSocket.
        /// </summary>
        public void Connect() {
            IPAddress ipAddress = IPAddress.Parse(serverIP);
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

            if (clientSocket == null) {
                clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            }
            else {
                if (!clientSocket.Connected)
                    clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            }

            clientSocket.Connect(remoteEP);

            receiver = new Thread(continuousReceiver);
            receiver.Start();
        }

        /// <summary>
        /// Disconnects the ClientSocket.
        /// </summary>
        public void Disconnect() {
            if (clientSocket != null && clientSocket.Connected) {
                Send(OpCode.DISCONNECT);
                clientSocket.Shutdown(SocketShutdown.Both);
                clientSocket.Close();
            }
        }

        /// <summary>
        /// Sends a message to the server with the specified OpCode and arguments.
        /// </summary>
        /// <param name="opCode">Message Operation Code.</param>
        /// <param name="args">Message arguments for the selected OpCode.</param>
        public override void Send(OpCode opCode, params string[] args) {
            send(MessageHandler.CreateMessage(opCode, args));
        }

        /// <summary>
        /// Sends a message to the server with the specified ErrorCode and arguments.
        /// </summary>
        /// <param name="opcode">Message Operation Code.</param>
        /// <param name="args">Message arguments for the selected OpCode.</param>
        public override void Send(ErrorCode errorCode, params string[] args) {
            send(MessageHandler.CreateMessage(errorCode, args));
        }

        /// <summary>
        /// Sends a message to the server.
        /// </summary>
        /// <param name="message"></param>
        protected override void send(string message) {
            byte[] sendBytes = Encoding.ASCII.GetBytes(message);

            if (clientSocket != null && clientSocket.Connected) {
                clientSocket.Send(sendBytes);
            }
        }

        /// <summary>
        /// Listens from new messages from the server.
        /// </summary>
        /// <returns>Message received from server.</returns>
        public override string Receive() {
            byte[] bytesFrom = new byte[1024];
            string message = null;

            if (clientSocket.Connected) {

                clientSocket.Receive(bytesFrom);

                message = Encoding.ASCII.GetString(bytesFrom).TrimEnd((char)0);

                if (!message.Equals("")) {
                    lock (messagesReceived) {
                        messagesReceived.Add(message);
                    }
                }
            }

            return message;
        }

        /// <summary>
        /// This function is executed on a thread. It listens the server continuously.
        /// </summary>
        private void continuousReceiver() {
            while (clientSocket.Connected) {
                Receive();
            }
            Debug.Log(">> STOP RECEIVING MESSAGES");
        }

        /// <summary>
        /// Returns one message from messagesReceived list for the specified OpCode.
        /// </summary>
        /// <param name="opCode">Desired OpCode to search messages of.</param>
        /// <returns>Message in string format for the desired OpCode</returns>
        public string GetReceivedMessage(OpCode opCode) {
            string output = null;
            lock (messagesReceived) {
                for (int i = 0; i < messagesReceived.Count; i++) {
                    if (MessageHandler.ReadOpCode(messagesReceived[i]) == opCode) {
                        string message = messagesReceived[i];
                        messagesReceived.RemoveAt(i);
                        output = message;
                        break;
                    }
                }
            }
            return output;
        }

        /*public void AddMessage(OpCode opcode, params string[] args) {
            string message = MessageHandler.CreateMessage(opcode, args);
            lock (messagesReceived) {
                messagesReceived.Add(message);
            }
        }*/

        /// <summary>
        /// Adds a message to the messagesReceived list. Used only to auto-receive errors.
        /// </summary>
        /// <param name="message"></param>
        public void AddMessage(ErrorCode errorCode, params string[] args) {
            addMessage(MessageHandler.CreateMessage(errorCode, args));
        }

        /// <summary>
        /// Adds a message to the messagesReceived list.
        /// </summary>
        /// <param name="message"></param>
        private void addMessage(string message) {
            lock (messagesReceived) {
                messagesReceived.Add(message);
            }
        }

        public void CreatePlayerProfile(string name, int userID, int currentExp) {
            playerProfile = new PlayerProfile(name, userID, currentExp);
            Debug.Log(playerProfile.ToString());
        }

        protected override void receive() {
            throw new NotImplementedException();
        }
    }
}
