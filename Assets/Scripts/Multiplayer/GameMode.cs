﻿namespace TFGFran.Game {
    public enum GameMode {
        T_VS_T__DEATHMATCH,
        T_VS_T__CAPTURE_THE_FLAG,
        T_VS_T__POINT_OF_CONTROL,
        A_VS_A__DEATHMATCH,
        A_VS_A__VIRUS,
        A_VS_A__SUITCASE,
        A_VS_A__TELEPORT
    }
}
