﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;

namespace TFGFran.Player {
    public class PlayerMovement : NetworkBehaviour {

        public BulletInfo Bullet;

        void Update() {
            if(isLocalPlayer)
                if(Input.GetKeyDown(KeyCode.Space)) {
                    Vector3 spawnPos = transform.position + transform.forward * 0.4f;
                    CmdShoot(spawnPos, transform.forward, GetComponent<PlayerInfo>().netId);
                }
        }

        [Command]
        private void CmdShoot(Vector3 position, Vector3 direction, NetworkInstanceId playerNetID) {

            BulletInfo bulletInfo = Instantiate(Bullet, position, Quaternion.LookRotation(direction)) as BulletInfo;
            GameObject player = ClientScene.FindLocalObject(playerNetID);
            NetworkServer.SpawnWithClientAuthority(bulletInfo.gameObject, player);
            bulletInfo.RpcSetPlayer(playerNetID);
            bulletInfo.RpcApplyforce(direction * 800);
        }

        void FixedUpdate() {
            if (isLocalPlayer) {
                int mod = 1;
                if (Input.GetKey(KeyCode.LeftShift)) {
                    mod = 2;
                }
                transform.Translate(Vector3.forward * Input.GetAxis("Vertical") * Time.fixedDeltaTime * mod);
                transform.Rotate(new Vector3(0, Input.GetAxis("Horizontal") * 2 * mod, 0));
            }
        }
    }
}