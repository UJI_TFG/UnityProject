﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using TFGFran.Networking;
using TFGFran.Game;
using System;
using UnityEngine.Networking.Match;
using TFGFran.MatchMaking;
using TFGCommonLib.Networking;

namespace TFGFran.Player {

    public class PlayerInfo : NetworkBehaviour {

        public UpdatePlayerCanvas upc;

        public int MaxHealth = 100;
        [SyncVar]
        private int currentHealth = 100;

        [SyncVar]
        public int TeamID = -1;

        [SyncVar]
        public string PlayerName = "PLAYER NAME";

        public Vector3 DespawnPos;

        public int PersonalScore = 0;
        public int Kills = 0;
        public int Deaths = 0;

        public int PlayerID = -1;

        private bool dead = false;
        private int secondsToRespawn = 3;
        private float respawnTimer = 0;

        // Use this for initialization
        public override void OnStartLocalPlayer() {
            SetName();
            SetPlayerID(ConnectionManager.Instance.PlayerProfile.ID);
        }

        void Update() {
            if (dead) {
                if (respawnTimer < secondsToRespawn)
                    respawnTimer += Time.deltaTime;
                else {
                    dead = false;
                    respawnTimer = 0;

                    FindObjectOfType<GameModeManager>().SpawnPlayer(this);
                }
            }

        }

        public void SetName() {
            CmdSetNamePlayer(ConnectionManager.Instance.PlayerProfile.Name);
        }

        public void SetPlayerID(int playerID) {
            CmdSetPlayerID(playerID);
        }

        public void SetColor(int teamID) {
            GetComponent<Renderer>().material.color = FindObjectOfType<GameModeManager>().TeamColours[teamID];
        }

        public void UpdateInDatabase() {
            ConnectionManager.Instance.Send(OpCode.UPDATE_MATCH_PLAYER, FindObjectOfType<GameModeManager>().MatchID.ToString(), PersonalScore.ToString(), Kills.ToString(), Deaths.ToString());
        }

        [Command]
        private void CmdSetNamePlayer(string name) {
            RpcSetNamePlayer(name);
            PlayerName = name;
        }

        [Command]
        private void CmdSetPlayerID(int playerID) {
            RpcSetPlayerID(playerID);
        }

        [Command]
        public void CmdGetTeam() {
            TeamID = FindObjectOfType<GameModeManager>().GetBestTeam();

            SetColor(TeamID);

            RpcSetTeam(TeamID);
        }

        [Command]
        public void CmdTakeDamage(int amount, NetworkInstanceId fromNetId) {
            currentHealth -= amount;
            RpcSetCurrentHealth(currentHealth);

            if (currentHealth <= 0) {
                CmdKillPlayer();
                PlayerInfo pi = NetworkServer.FindLocalObject(fromNetId).GetComponent<PlayerInfo>();
                pi.CmdAddScore(10);
                RpcAddTeamScore(pi.TeamID);
            }
        }

        [ClientRpc]
        private void RpcAddTeamScore(int team) {
            FindObjectOfType<MatchManager>().AddScore(1, team);
        }

        [Command]
        private void CmdAddScore(int amount) {
            PersonalScore += amount;
            RpcSetScore(PersonalScore);
        }

        [Command]
        public void CmdResetDamage() {
            currentHealth = MaxHealth;
            RpcSetCurrentHealth(currentHealth);
        }

        [Command]
        public void CmdSpawnPlayer(Vector3 position) {
            //SpawnPlayer(position);
            RpcSpawnPlayer(position);
        }

        [Command]
        public void CmdKillPlayer() {
            RpcKillPlayer();
            RpcDespawnPlayer(DespawnPos);
        }

        [Command]
        public void CmdDespawnPlayer(Vector3 position) {
            RpcDespawnPlayer(position);
        }

        [ClientRpc]
        private void RpcSetNamePlayer(string name) {
            PlayerName = name;
            upc.RpcUpdatePlayerName(name);
        }

        [ClientRpc]
        private void RpcSetPlayerID(int playerID) {
            PlayerID = playerID;
        }

        [ClientRpc]
        public void RpcSetTeam(int newTeam) {
            TeamID = newTeam;

            SetColor(TeamID);
        }

        [ClientRpc]
        private void RpcSetScore(int score) {
            PersonalScore = score;
            Kills++;
            if (isLocalPlayer)
                UpdateInDatabase();
        }

        [ClientRpc]
        public void RpcSetCurrentHealth(int health) {
            currentHealth = health;
            upc.RpcUpdateHealthBar(health);
        }

        [ClientRpc]
        public void RpcSpawnPlayer(Vector3 position) {
            spawnPlayer(position);
        }

        [ClientRpc]
        public void RpcDespawnPlayer(Vector3 position) {
            despawnPlayer(position);
        }

        [ClientRpc]
        public void RpcKillPlayer() {
            respawnTimer = 0;
            Deaths++;
            if (isLocalPlayer)
                UpdateInDatabase();
            dead = true;
        }

        private void spawnPlayer(Vector3 position) {
            CmdResetDamage();

            GetComponent<BoxCollider>().enabled = true;
            GetComponent<PlayerMovement>().enabled = true;
            GetComponent<Rigidbody>().useGravity = true;

            transform.position = position;

            if (isLocalPlayer) {
                Camera.main.transform.parent = transform;
                Camera.main.transform.position = transform.FindChild("cameraPos").position;
                Camera.main.transform.LookAt(transform);
            }
        }

        private void despawnPlayer(Vector3 position) {
            CmdResetDamage();

            GetComponent<BoxCollider>().enabled = false;
            GetComponent<PlayerMovement>().enabled = false;
            GetComponent<Rigidbody>().useGravity = false;

            transform.position = position;

            if (isLocalPlayer) {
                Camera.main.transform.parent = null;
                Camera.main.transform.position = new Vector3(0, 20, 0);
                Camera.main.transform.rotation = Quaternion.Euler(90, 0, 0);
            }
        }

        public void DropConnection() {
            MatchMaker matchMaker = FindObjectOfType<MatchMaker>();
            matchMaker.DropConnection();
        }

        [ClientRpc]
        public void RpcAddExp(int expRate) {
            ConnectionManager.Instance.PlayerProfile.AddExp(PersonalScore, expRate);
        }
    }
}