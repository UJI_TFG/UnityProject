﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using TFGFran.Game;
using System;
using TFGFran.Player;

public class BulletInfo : NetworkBehaviour {

    public PlayerInfo Player;

    public void SetColor(int teamID) {
        gameObject.GetComponent<Renderer>().material.color = FindObjectOfType<GameModeManager>().TeamColours[teamID];
    }

    [ClientRpc]
    public void RpcSetPlayer(NetworkInstanceId playerNetID) {
        Player = ClientScene.FindLocalObject(playerNetID).GetComponent<PlayerInfo>();

        SetColor(Player.TeamID);
    }

    [ClientRpc]
    public void RpcApplyforce(Vector3 force) {
        GetComponent<Rigidbody>().AddForce(force);
    }

    void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player"))
            if(other.GetComponent<PlayerInfo>().TeamID != Player.TeamID) {
                other.GetComponent<PlayerInfo>().CmdTakeDamage(10, Player.netId);
            }
        if(isServer)
            NetworkServer.Destroy(gameObject);
    }
}
