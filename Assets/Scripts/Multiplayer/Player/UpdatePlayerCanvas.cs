﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;

public class UpdatePlayerCanvas : NetworkBehaviour {

    private Slider bar;
    private TextMesh text;

    // Use this for initialization
    void Start () {
        bar = GetComponentInChildren<Slider>();
        text = GetComponent<TextMesh>();
    }

    // Update is called once per frame
    void Update() {
        transform.LookAt(-Camera.main.transform.position);
    }


    public void RpcUpdateHealthBar(int value) {
        bar.value = value;
    }

    public void RpcUpdatePlayerName(string name) {
        text.text = name;
    }
}
