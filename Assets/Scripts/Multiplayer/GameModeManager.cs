﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections.Generic;
using TFGFran.Game;
using TFGFran.Player;
using TFGFran.MatchMaking;
using TFGFran.Networking;
using TFGCommonLib.Networking;
using TFGCommonLib.Utils;

namespace TFGFran.Game {
    public class GameModeManager : NetworkBehaviour {

        public GameObject MazeMapPrefab;
        public GameObject MazeStartPoints;
        public GameObject MatchManagerDM;

        [SyncVar]
        private MatchStatus matchStatus = MatchStatus.WAITING_PLAYERS;

        public int MinNumPlayers = 2;

        [SyncVar]
        private GameMode currentGameMode;
        private bool gameSet = false;

        private int currentPlayers = 0;

        [SyncVar]
        private int numberOfTeams = 2;
        private int[] teams;
        public Color[] TeamColours = new Color[10];

        [SyncVar]
        public int MatchID = -1;

        private bool started = false;
        private bool playerRegistered = false;
        private bool oneTime = false;

        private List<PlayerInfo> playerList = new List<PlayerInfo>();

        private InterfaceManager interfaceManager;
        private MatchManager matchManager;

        private PlayerInfo localPlayer;

        // Use this for initialization
        public void Start() {

            CmdGetRandomGameMode();

            getInterfaceManager();
            interfaceManager.SetGameModeText(currentGameMode);

            if (isServer)
                ConnectionManager.Instance.Send(OpCode.CREATE_MATCH, (((int)currentGameMode) + 1).ToString());

            if (NetworkServer.active) {
                GameObject go = Instantiate(MazeMapPrefab, Vector3.zero, Quaternion.identity) as GameObject;
                NetworkServer.Spawn(MazeMapPrefab);
                GameObject sp = Instantiate(MazeStartPoints, Vector3.zero, Quaternion.identity) as GameObject;
                NetworkServer.Spawn(MazeStartPoints);
            }

        }

        private void getInterfaceManager() {
            if (!interfaceManager)
                interfaceManager = GetComponent<InterfaceManager>();
        }

        [Command]
        private void CmdGetRandomGameMode() {

            GameMode[] availableGameModes;

            if (MatchMaker.TeamMatch) {
                availableGameModes = new GameMode[1 /*3*/] {
                /*GameMode.T_VS_T__CAPTURE_THE_FLAG,*/
                GameMode.T_VS_T__DEATHMATCH/*,
                GameMode.T_VS_T__POINT_OF_CONTROL*/
                };

                numberOfTeams = 2;
            }
            else {
                availableGameModes = new GameMode[4] {
                GameMode.A_VS_A__DEATHMATCH,
                GameMode.A_VS_A__SUITCASE,
                GameMode.A_VS_A__TELEPORT,
                GameMode.A_VS_A__VIRUS
                };

                numberOfTeams = 10;
            }

            teams = new int[numberOfTeams];

            if (!gameSet) {
                currentGameMode = availableGameModes[Random.Range(0, availableGameModes.Length)];
                gameSet = true;
            }

            RpcSetGameMode(currentGameMode);
        }

        [ClientRpc]
        public void RpcSetGameMode(GameMode gm) {
            currentGameMode = gm;
            teams = new int[numberOfTeams];
        }

        // Update is called once per frame
        public void Update() {
            CheckForPlayers();

            if (MatchID == -1)
                checkMatchID();
            else {
                if (!playerRegistered) {
                    playerRegistered = true;
                    ConnectionManager.Instance.Send(OpCode.ADD_MATCH_PLAYER, FindObjectOfType<GameModeManager>().MatchID.ToString());
                }
            }

            switch (matchStatus) {
                default:
                    break;
                case MatchStatus.WAITING_PLAYERS:
                    currentPlayers = NetworkServer.connections.Count;

                    if (NetworkServer.connections.Count < MinNumPlayers && playerList.Count < MinNumPlayers) {
                        NetworkStartPosition[] spawnPoints = FindObjectsOfType<NetworkStartPosition>();
                        localPlayer = getLocalPlayer();
                        if (localPlayer != null && spawnPoints != null)
                            localPlayer.transform.position = spawnPoints[0].transform.position;
                    }
                    else {
                        if (isServer)
                            RpcSetMatchStatus(MatchStatus.PREPARING);
                    }
                    break;
                case MatchStatus.FINISHED:
                    if (!oneTime) {
                        localPlayer = getLocalPlayer();
                        if (localPlayer != null)
                            localPlayer.CmdDespawnPlayer(localPlayer.DespawnPos);


                        if (isServer) {

                            sendInfoToDatabase();

                            RpcSetMatchStatus(MatchStatus.RESULTS);
                        }

                        interfaceManager.ShowDCButton();
                        interfaceManager.ShowWinner();

                        oneTime = true;
                    }
                    break;
            }
        }

        private void sendInfoToDatabase() {
            int expRate = 1;

            ConnectionManager.Instance.Send(OpCode.REQUEST_EXP_RATE);
            string message = null;
            while (message == null) {
                message = ConnectionManager.Instance.GetReceivedMessage(OpCode.EXP_RATE);
            }
            expRate = int.Parse(MessageHandler.ReadParams(message)[0]);

            localPlayer.RpcAddExp(expRate);

            ConnectionManager.Instance.Send(OpCode.UPDATE_MATCH, MatchID.ToString(), DateTimeUtils.DateTimeToTimestamp(System.DateTime.UtcNow).ToString(), matchManager.GetWinnerTeam().ToString());
        }

        public void CheckForPlayers() {
            playerList.Clear();
            teams = new int[numberOfTeams];

            List<int> playerIDS = new List<int>();

            GameObject[] playersFound = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in playersFound) {
                PlayerInfo pi = player.GetComponent<PlayerInfo>();
                if (pi) {
                    if (!playerIDS.Contains(pi.PlayerID)) {
                        playerIDS.Add(pi.PlayerID);
                        playerList.Add(pi);
                        if (pi.TeamID != -1)
                            teams[pi.TeamID]++;
                    }
                    else {
                        CmdDisconnectPlayer(pi.netId);
                    }

                }
            }
            Debug.Log("Players added: " + playerList.Count);

            foreach (PlayerInfo player in playerList) {
                if (player.TeamID == -1)
                    player.CmdGetTeam();
                player.SetColor(player.TeamID);
            }

            if (matchStatus == MatchStatus.PREPARING || matchStatus == MatchStatus.PLAYING)
                StartMatch();

            if (isServer && matchManager)
                RpcSyncManagerStatus(matchManager.Timer, matchManager.MatchStarted, matchManager.Scores);
        }

        private void checkMatchID() {
            string message = ConnectionManager.Instance.GetReceivedMessage(OpCode.MATCH_ID);
            if (message != null)
                MatchID = int.Parse(MessageHandler.ReadParams(message)[0]);
        }

        public void StartMatch() {
            if (!started) {

                localPlayer = getLocalPlayer();

                if (localPlayer) {
                    SpawnPlayer(localPlayer);

                    //Spawn match manager
                    GameObject MM = Instantiate(MatchManagerDM);
                    matchManager = MM.GetComponent<MatchManager>();
                    matchManager.SetTeams(teams.Length);
                    matchManager.StartTimer();

                    started = true;
                }
            }
        }

        [ClientRpc]
        public void RpcSyncManagerStatus(float timer, bool matchStarted, int[] scores) {
            if (!isServer && matchManager) {
                matchManager.SetMatchStatus(timer, matchStarted);
                matchManager.Scores = scores;
            }
        }

        public void SpawnPlayer(PlayerInfo pi) {
            GameObject[] respawns = GameObject.FindGameObjectsWithTag("Respawn");

            if (MatchMaker.TeamMatch) {
                Vector3 offset = new Vector3(Random.Range(0, 1.5f), 0, Random.Range(0, 1.5f));

                foreach (GameObject go in respawns) {
                    if (go.name == "NeutralSpawn")
                        pi.DespawnPos = go.transform.position;
                    else if (go.name == "RedSpawn" && pi.TeamID == 0)
                        pi.CmdSpawnPlayer(go.transform.position + offset);
                    else if (go.name == "BlueSpawn" && pi.TeamID == 1)
                        pi.CmdSpawnPlayer(go.transform.position + offset);
                }
            }
            else {
                //random
            }
        }

        private PlayerInfo getLocalPlayer() {
            foreach (PlayerInfo player in playerList) {
                if (player && player.isLocalPlayer)
                    return player;
            }

            return null;
        }

        public int GetBestTeam() {
            int best = 0;
            for (int i = 1; i < teams.Length; i++) {
                if (teams[i] < teams[best])
                    best = i;
            }

            teams[best]++;

            return best;
        }

        [ClientRpc]
        public void RpcSetMatchStatus(MatchStatus ms) {
            matchStatus = ms;
        }

        [Command]
        public void CmdDisconnectPlayer(NetworkInstanceId dcNetID) {
            RpcDisconnectPlayer(dcNetID);
        }

        [ClientRpc]
        public void RpcDisconnectPlayer(NetworkInstanceId dcNetID) {
            localPlayer = getLocalPlayer();
            if (localPlayer != null)
                if (localPlayer.netId == dcNetID) {
                    localPlayer.DropConnection();
                }
        }

        public PlayerInfo[] GetCurrentPlayers() {
            PlayerInfo[] copy = new PlayerInfo[playerList.Count];
            playerList.CopyTo(copy);

            return copy;
        }

        public MatchStatus GetMatchStatus() {
            return matchStatus;
        }
    }
}