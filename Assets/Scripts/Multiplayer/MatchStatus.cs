﻿namespace TFGFran.Game {
    public enum MatchStatus {
        WAITING_PLAYERS,
        PREPARING,
        PLAYING,
        FINISHED,
        RESULTS
    }
}