﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;

namespace TFGFran.MatchMaking {

    [RequireComponent(typeof(NetworkManager))]
    public class MatchMaker : MonoBehaviour {

        private NetworkManager networkManager;

        private uint matchSize = 10;
        private string singleMatchName = "TFGRoom Single {0}";
        private string teamMatchName = "TFGRoom Team {0}";

        public static bool TeamMatch = false;

        private bool connected = false;

        public MatchInfo MatchInfo;

        public void Awake() {
            networkManager = GetComponent<NetworkManager>();
        }

        public void FindTeamMatch() {
            TeamMatch = true;

            findMatch();
        }

        public void FindSingleMatch() {
            TeamMatch = false;

            findMatch();
        }


        private void findMatch() {
            networkManager.StartMatchMaker();
            networkManager.matches = null;

            if (networkManager.matchInfo == null) {
                if (networkManager.matches == null) {
                    string matchFilter = "Single";
                    if (TeamMatch)
                        matchFilter = "Team";
                    networkManager.matchMaker.ListMatches(0, 20, matchFilter, onMatchList);
                }
            }
        }

        private bool joinToAnyMatch() {

            foreach (MatchDesc match in networkManager.matches) {
                if (match.currentSize < matchSize) {
                    //networkManager.matchMaker.DestroyMatch(match.networkId, onDestroyMatch);
                    networkManager.matchMaker.JoinMatch(match.networkId, "", onMatchJoined);
                    return true;
                }
            }
            return false;
        }

        public void DropConnection() {
            NetworkMatch match = FindObjectOfType<NetworkMatch>();
            match.DropConnection(MatchInfo.networkId, MatchInfo.nodeId, onConnectionDropped);
        }

        //Callbacks

        private void onDestroyMatch(BasicResponse resp) {
            Debug.Log("Destroy: " + resp.success);
        }

        private void onMatchList(ListMatchResponse matchList) {

            networkManager.matches = matchList.matches;

            Debug.Log("Found " + networkManager.matches.Count + " matches");

            if (networkManager.matches.Count == 0) {

                if (TeamMatch)
                    networkManager.matchMaker.CreateMatch(string.Format(teamMatchName, networkManager.matches.Count), matchSize, true, "", onMatchCreate);
                else
                    networkManager.matchMaker.CreateMatch(string.Format(singleMatchName, networkManager.matches.Count), matchSize, true, "", onMatchCreate);
            }


            else {
                if (!joinToAnyMatch()) {
                    if (TeamMatch)
                        networkManager.matchMaker.CreateMatch(string.Format(teamMatchName, networkManager.matches.Count), matchSize, true, "", onMatchCreate);
                    else
                        networkManager.matchMaker.CreateMatch(string.Format(singleMatchName, networkManager.matches.Count), matchSize, true, "", onMatchCreate);
                }
            }
        }

        private void onMatchCreate(CreateMatchResponse matchInfo) {

            MatchInfo = new MatchInfo(matchInfo);

            if (matchInfo.success) {
                Debug.Log("Match created");
                Utility.SetAccessTokenForNetwork(matchInfo.networkId, new NetworkAccessToken(matchInfo.accessTokenString));
                connected = true;
                networkManager.StartHost(new MatchInfo(matchInfo));
            }
            else {
                Debug.LogError("Create Failed:" + matchInfo);
                return;
            }
        }

        private void onMatchJoined(JoinMatchResponse matchInfo) {

            MatchInfo = new MatchInfo(matchInfo);

            if (matchInfo.success) {
                Debug.Log("Joining...");
                Utility.SetAccessTokenForNetwork(matchInfo.networkId, new NetworkAccessToken(matchInfo.accessTokenString));
                connected = true;
                networkManager.StartClient(new MatchInfo(matchInfo));
            }
            else {
                Debug.LogError("Join Failed:" + matchInfo);
            }
        }

        private void onConnectionDropped(BasicResponse callback) {
            if (callback.success) {
                if (connected) {
                    Debug.Log("Connection has been dropped on matchmaker server");
                    MatchInfo = null;
                    Destroy(FindObjectOfType<NetworkMatch>());

                    networkManager.StopClient();
                    networkManager.StopMatchMaker();

                    NetworkTransport.Shutdown();

                    connected = false;
                }
                else
                    Debug.Log("Not connected");
            }
            else
                Debug.Log("Drop failed");
        }
    }
}