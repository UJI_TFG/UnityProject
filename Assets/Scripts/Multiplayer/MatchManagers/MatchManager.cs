﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;

namespace TFGFran.Game {
    public class MatchManager : MonoBehaviour {

        public int[] Scores;

        public float Timer = 0;
        private int startTime = 5;

        private int matchTime = 60;

        public bool MatchStarted = false;

        private InterfaceManager interfaceManager;
        private GameModeManager gameManager;

        void Awake() {
            interfaceManager = FindObjectOfType<InterfaceManager>();
            gameManager = FindObjectOfType<GameModeManager>();

            interfaceManager.SetMatchTime(matchTime);
            interfaceManager.ShowIngameUI();
        }

        void Update() {
            if (!MatchStarted) {
                if (Timer > -1) {
                    Timer -= Time.deltaTime;
                    interfaceManager.SetStartTimer((int)Timer);
                }
                else {
                    Timer = matchTime;
                    interfaceManager.HideStartTimer();
                    MatchStarted = true;
                    if (gameManager.isServer)
                        gameManager.RpcSetMatchStatus(MatchStatus.PLAYING);
                }
            }
            else {
                if(Timer > 0) {
                    Timer -= Time.deltaTime;
                    interfaceManager.SetMatchTime((int)Timer);
                }
                else {
                    interfaceManager.SetMatchTime(0);
                    if (gameManager.isServer) {
                        gameManager.RpcSetMatchStatus(MatchStatus.FINISHED);
                    }
                }
                    
            }
        }

        public void SetTeams(int number) {
            Scores = new int[number];
        }

        public void SetMatchStatus(float time, bool started) {
            MatchStarted = started;
            if(started)
                interfaceManager.HideStartTimer();
            Timer = time;
        }

        public void AddScore(int amount, int teamID) {
            Scores[teamID] += amount;
            interfaceManager.SetTeamScore(Scores);
        }

        public void StartTimer() {
            Timer = startTime;
            interfaceManager.SetStartTimer(startTime);
        }

        public int GetWinnerTeam() {
            int maxIndex = 0;
            for(int i = 0; i < Scores.Length; i++) {
                if (Scores[i] > Scores[maxIndex])
                    maxIndex = i;
            }
            return maxIndex;
        }
    }
}