﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TFGFran.Player;

public class PlayerResults : MonoBehaviour {

    public Text PlayerName;
    public Text Kills;
    public Text Deaths;
    public Text Score;
    public PlayerInfo PlayerInfo;
	
	void Start () {
	    if(PlayerInfo) {
            PlayerName.text = PlayerInfo.PlayerName;
            Kills.text = string.Format("{0} kills.",PlayerInfo.Kills);
            Deaths.text = string.Format("{0} deaths.", PlayerInfo.Deaths);
            Score.text = string.Format("{0} points.", PlayerInfo.PersonalScore);
        }
	}
}
