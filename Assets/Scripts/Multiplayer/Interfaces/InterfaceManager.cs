﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TFGFran.MatchMaking;
using TFGFran.Game;
using TFGFran.Player;
using System;

public class InterfaceManager : MonoBehaviour {

    public Text GameModeText;

    public Text MatchTime;
    public Text PersonalScore;
    public Text TeamScore;
    public Text StartTimer;
    public Text WinnerText;
    public GameObject StartTimerObject;
    public GameObject IngameUI;
    public GameObject DCButton;
    public GameObject ResultsScreen;
    public GameObject ResultsList;
    public GameObject PlayerResultsPrefab;


    public void SetGameModeText(GameMode gamemode) {
        GameModeText.enabled = true;
        if (gamemode == GameMode.T_VS_T__CAPTURE_THE_FLAG) {
            GameModeText.text = "Capture the flag (Teams)";
        }
        else if (gamemode == GameMode.T_VS_T__DEATHMATCH) {
            GameModeText.text = "Deathmatch (Teams)";
        }
        else if (gamemode == GameMode.T_VS_T__POINT_OF_CONTROL) {
            GameModeText.text = "Point of control (Teams)";
        }
        else if (gamemode == GameMode.A_VS_A__DEATHMATCH) {
            GameModeText.text = "Deathmatch (All vs All)";
        }
        else if (gamemode == GameMode.A_VS_A__SUITCASE) {
            GameModeText.text = "Suitcase carrier (All vs All)";
        }
        else if (gamemode == GameMode.A_VS_A__TELEPORT) {
            GameModeText.text = "Teleporter (All vs All)";
        }
        else if (gamemode == GameMode.A_VS_A__VIRUS) {
            GameModeText.text = "Virus (All vs All)";
        }
    }

    public void ShowIngameUI() {
        IngameUI.SetActive(true);
    }

    public void SetStartTimer(int seconds) {
        if (seconds == -1)
            StartTimerObject.SetActive(false);
        StartTimer.text = seconds.ToString();
    }

    public void HideStartTimer() {
        StartTimerObject.SetActive(false);
    }

    public void SetMatchTime(int time) {
        MatchTime.text = string.Format("{0}:{1:00}", time / 60, time % 60);
    }

    public void SetTeamScore(int[] scores) {
        TeamScore.text = string.Format("{0} : {1}", scores[0], scores[1]);
    }

    public void DisconnectButton() {
        FindObjectOfType<MatchMaker>().DropConnection();
    }

    public void ShowResults(bool active) {
        PlayerInfo[] players = FindObjectOfType<GameModeManager>().GetCurrentPlayers();

        foreach (Transform child in ResultsList.transform) {
            Destroy(child.gameObject);
        }

        foreach (PlayerInfo player in players) {
            GameObject go = Instantiate(PlayerResultsPrefab);
            go.GetComponent<PlayerResults>().PlayerInfo = player;
            go.transform.parent = ResultsList.transform;
        }

        ResultsScreen.SetActive(active);
    }

    public void ShowDCButton() {
        DCButton.SetActive(true);
    }

    void Update() {
        MatchStatus ms = FindObjectOfType<GameModeManager>().GetMatchStatus();
        if (ms == MatchStatus.PLAYING || ms == MatchStatus.PREPARING) {
            ShowResults(Input.GetKey(KeyCode.Tab));
        }
        else
            ShowResults(true);
    }

    public void ShowWinner() {
        int win = FindObjectOfType<MatchManager>().GetWinnerTeam();
        WinnerText.text = string.Format("Team {0} wins!", win);
        WinnerText.color = FindObjectOfType<GameModeManager>().TeamColours[win];
        WinnerText.enabled = true;
    }
}