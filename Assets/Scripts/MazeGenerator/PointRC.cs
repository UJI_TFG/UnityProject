﻿using System;

[Serializable]
public struct PointRC : IEquatable<PointRC> {
    public int Row, Column;
    public PointRC(int r, int c) {
        Row = r;
        Column = c;
    }

    public bool Equals(PointRC other) {
        if (Column == other.Column && Row == other.Row)
            return true;
        return false;
    }

    public override string ToString() {
        return "(" + Row + ", " + Column + ")";
    }
}