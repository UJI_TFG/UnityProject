﻿using UnityEngine;
using UnityEngine.Networking;

public class MazeHierarchy : NetworkBehaviour {

	void Start () {
        GameObject parent = GameObject.Find("Maze");

        if(transform.parent != parent) {
            transform.parent = parent.transform;
        }

        Destroy(this);
	}
}
