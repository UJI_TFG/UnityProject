﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class MazeRooms : NetworkBehaviour {
    [System.Serializable]
    public class Cell {
        public PointRC Position;
        public bool Visited = false;
        public GameObject North; //1
        public GameObject East; //2
        public GameObject West;  //3
        public GameObject South; //4
        public bool IsRoom = false;

        public Cell(int row, int col) {
            Position = new PointRC(row, col);
        }

        public override string ToString() {
            return (IsRoom ? "true" : "false");
        }

        public void SetActive(bool active) {
            North.SetActive(active);
            East.SetActive(active);
            West.SetActive(active);
            South.SetActive(active);
        }
    }

    [System.Serializable]
    public struct Room {
        public PointRC RoomPos;
        private Size roomSize;
        public Size RoomSize {
            get {
                return roomSize;
            }
            set {
                roomSize = value;
                NorthWalls = new GameObject[value.Width];
                EastWalls = new GameObject[value.Height];
                WestWalls = new GameObject[value.Height];
                SouthWalls = new GameObject[value.Width];
            }
        }
        public GameObject[] NorthWalls; //1
        public GameObject[] EastWalls; //2
        public GameObject[] WestWalls;  //3
        public GameObject[] SouthWalls; //4
    }

    [System.Serializable]
    public struct Size {
        public int Width, Height;
        public Size(int w, int h) {
            Width = w;
            Height = h;
        }

        public override string ToString() {
            return "(" + Width + ", " + Height + ")";
        }
    }

    //Array que contiene los muros prefab (con los que se crea el laberinto)
    public GameObject[] PrefabWalls;
    public GameObject[] PrefabDoorWalls;
    public GameObject GroundPrefab;
    //Longitud de los muros
    private float wallLength = 1.0f;
    //Tamaño del laberinto (en celdas)
    public PointRC MazeSize = new PointRC(5, 5);
    //Posicion inicial para los muros (para hacer que el laberinto esté centrado)
    private Vector3 initialPos;
    //Array con las celdas
    private Cell[,] cellMatrix;
    private int totalCells;
    private int visitedCells = 0;

    //Cantidad de salas
    public int RoomCount = 2;
    //tamaño minimo y maximo para los lados de las salas
    public int MaxRoomSize = 3;
    public int MinRoomSize = 2;
    //Array con las salas
    private Room[] rooms = new Room[0];

    //Variables para el laberinto
    private PointRC neighbourPos;
    private int wallToBreak;

    //Spawn doors
    public bool CreateSpawnDoors = false;

    /// <summary>
    /// Unity initialization
    /// </summary>
    public void Start() {

        name = "Maze";

        createWalls();
        createCells();
        createRooms();
        processRoomWalls();
        createMaze();
        spawnFloor();

        if (CreateSpawnDoors) {
            createMazeEntries();
        }

        //Destroy(this);
    }

    [Server]
    private void createMazeEntries() {
        GameObject go = cellMatrix[MazeSize.Row / 2, 0].West;
        GameObject spawned;
        Transform t;

        t = go.transform;
        NetworkServer.Destroy(go);
        spawned = Instantiate(PrefabDoorWalls[0], t.position, t.rotation) as GameObject;
        spawned.transform.parent = transform;
        NetworkServer.Spawn(spawned);

        go = cellMatrix[MazeSize.Row / 2, MazeSize.Column - 1].East;

        t = go.transform;
        NetworkServer.Destroy(go);
        spawned = Instantiate(PrefabDoorWalls[0], t.position, t.rotation) as GameObject;
        spawned.transform.parent = transform;
        NetworkServer.Spawn(spawned);
    }

    /// <summary>
    /// Server only function.
    /// Spawns the floor object.
    /// </summary>
    [Server]
    private void spawnFloor() {
        GameObject floor = Instantiate(GroundPrefab, new Vector3(0, -0.5f, 0), Quaternion.identity) as GameObject;
        floor.transform.parent = transform;
        NetworkServer.Spawn(floor);
    }

    /// <summary>
    /// Server only function.
    /// Create and spawn all walls that pupulate the maze.
    /// </summary>
    [Server]
    private void createWalls() {
        initialPos = new Vector3(-MazeSize.Column / 2 + wallLength / 2 + transform.position.x,
            0.5f + transform.position.y,
            -MazeSize.Row / 2 + wallLength / 2 + transform.position.z);

        Vector3 wallWorldPosition = initialPos;
        GameObject tempWall;

        //Variables para elegir un muro aleatorio de los prefab que hay
        int rand = 0;

        //For muros x
        for (int i = 0; i < MazeSize.Row; i++) {
            for (int j = 0; j <= MazeSize.Column; j++) {

                //Se elige un muro aleatorio
                rand = Random.Range(0, PrefabWalls.Length);

                wallWorldPosition = new Vector3(initialPos.x + (j * wallLength) - wallLength / 2, initialPos.y, initialPos.z + (i * wallLength) - wallLength / 2);
                tempWall = Instantiate(PrefabWalls[rand], wallWorldPosition, Quaternion.identity) as GameObject;
                tempWall.transform.parent = transform;
                tempWall.name = "VERT-(x" + j + ",y" + i + ")";
                NetworkServer.Spawn(tempWall);
            }
        }

        //For muros y
        for (int i = 0; i <= MazeSize.Row; i++) {
            for (int j = 0; j < MazeSize.Column; j++) {

                rand = Random.Range(0, PrefabWalls.Length);

                wallWorldPosition = new Vector3(initialPos.x + (j * wallLength), initialPos.y, initialPos.z + (i * wallLength) - wallLength);
                tempWall = Instantiate(PrefabWalls[rand], wallWorldPosition, Quaternion.Euler(0, 90, 0)) as GameObject;
                tempWall.transform.parent = transform;
                tempWall.name = "HORI-(x" + j + ",y" + i + ")";
                NetworkServer.Spawn(tempWall);
            }
        }
    }

    /// <summary>
    /// Server only function.
    /// Starting from a gameObject with walls spawned with CreateWalls() function, creates a matrix with cells that contains 4 walls (N, S, W, E).
    /// </summary>
    [Server]
    private void createCells() {
        totalCells = MazeSize.Row * MazeSize.Column;
        cellMatrix = new Cell[MazeSize.Row, MazeSize.Column];

        GameObject[] wallList;
        int children = transform.childCount;
        wallList = new GameObject[children];

        //Get all children
        for (int i = 0; i < children; i++) {
            wallList[i] = transform.GetChild(i).gameObject;
        }

        //Assign walls to the cells
        for (int row = 0; row < cellMatrix.GetLength(0); row++) { //rows
            for (int col = 0; col < cellMatrix.GetLength(1); col++) { //columns

                cellMatrix[row, col] = new Cell(row, col);
                cellMatrix[row, col].West = wallList[col + row * (MazeSize.Column + 1)];
                cellMatrix[row, col].East = wallList[col + row * (MazeSize.Column + 1) + 1];

                cellMatrix[row, col].South = wallList[MazeSize.Row * (MazeSize.Column + 1) + col + row * MazeSize.Column];
                cellMatrix[row, col].North = wallList[MazeSize.Row * (MazeSize.Column + 1) + col + row * MazeSize.Column + MazeSize.Column];

                //Debug.Log("WEST r=" + row + " c=" + col + " op: " + (col + row * (mazeSize.x + 1)) + " WALL: " + wallList[col + row * (mazeSize.x + 1)].name);
                //Debug.Log("EAST r=" + row + " c=" + col + " op: " + (col + row * (mazeSize.x + 1) + 1) + " WALL: " + wallList[col + row * (mazeSize.x + 1) + 1].name);
                //Debug.Log("SOUTH r=" + row + " c=" + col + " op: " + (mazeSize.y * (mazeSize.x + 1) + col + row * mazeSize.x) + " WALL: " + wallList[mazeSize.y * (mazeSize.x + 1) + col + row * mazeSize.x].name);
                //Debug.Log("NORTH r=" + row + " c=" + col + " op: " + (mazeSize.y * (mazeSize.x + 1) + col + row * mazeSize.x + mazeSize.x) + " WALL: " + wallList[mazeSize.y * (mazeSize.x + 1) + col + row * mazeSize.x + mazeSize.x].name);
            }
        }

        //cellMatrix[0, 0].west.SetActive(false);


    }

    /// <summary>
    /// Server only function.
    /// Starting from a matrix of cells created with CreateCells() function, create some Rooms inside.
    /// </summary>
    [Server]
    private void createRooms() {
        #region serverRooms
        if (rooms.Length == 0) {
            PointRC roomPos;
            rooms = new Room[RoomCount];
            int currentRoomCount = 0;

            while (currentRoomCount < RoomCount) {

                Room tempRoom = new Room();
                Size roomSize = new Size(Random.Range(MinRoomSize, MaxRoomSize + 1), Random.Range(MinRoomSize, MaxRoomSize + 1));
                tempRoom.RoomSize = roomSize;

                //Debug.Log("SIZE " + roomSize);

                //Position (inverted x and y = row and column)
                roomPos = new PointRC(Random.Range(0, MazeSize.Row), Random.Range(0, MazeSize.Column));

                //Debug.Log("POS " + roomPos);

                //Check if the current cell is a room
                if (cellMatrix[roomPos.Row, roomPos.Column].IsRoom)
                    continue; //Stop this iteration

                //Border corrections
                //North
                if (roomPos.Row + roomSize.Height + 1 > MazeSize.Row) {
                    roomPos.Row = MazeSize.Row - roomSize.Height - 1;
                    //Debug.Log("Correction north: " + roomPos);
                }
                //South
                if (roomPos.Row == 0) {
                    roomPos.Row = 1;
                    //Debug.Log("Correction south: " + roomPos);
                }
                //East
                if (roomPos.Column + roomSize.Width + 1 > MazeSize.Column) {
                    roomPos.Column = MazeSize.Column - roomSize.Width - 1;
                    //Debug.Log("Correction east: " + roomPos);
                }
                //West
                if (roomPos.Column == 0) {
                    roomPos.Column = 1;
                    //Debug.Log("Correction west: " + roomPos);
                }

                //Check room overlap
                if (checkRoomOverlap(roomSize, roomPos)) //If current room overlaps other
                    continue; //Stop this iteration

                tempRoom.RoomPos = roomPos;

                rooms[currentRoomCount] = tempRoom;
                //rooms.Add(tempRoom);
                currentRoomCount++;

                //Set cells as room
                for (int row = 0; row < roomSize.Height; row++) {
                    for (int col = 0; col < roomSize.Width; col++) {
                        cellMatrix[row + roomPos.Row, col + roomPos.Column].IsRoom = true;
                        cellMatrix[row + roomPos.Row, col + roomPos.Column].Visited = true;
                    }
                }
            }
        }
        #endregion
    }

    /// <summary>
    /// Checks if two Room positions are overlapping
    /// </summary>
    /// <param name="roomSize">Room size.</param>
    /// <param name="roomPos">Room position.</param>
    /// <returns>Boolean with the overlap state.</returns>
    [Server]
    private bool checkRoomOverlap(Size roomSize, PointRC roomPos) {
        for (int row = 0; row < roomSize.Height; row++)
            for (int col = 0; col < roomSize.Width; col++)
                if (cellMatrix[row + roomPos.Row, col + roomPos.Column].IsRoom)
                    return true;
        return false;
    }

    /// <summary>
    /// Server only function.
    /// Process the walls of the rooms created on CreateRooms() function by enabling or disabling the corresponding ones. in addition, creates the doors that communicates the rooms with the maze.
    /// </summary>
    [Server]
    private void processRoomWalls() {
        for (int r = 0; r < rooms.Length; r++) {

            //Update visited cells
            visitedCells += rooms[r].RoomSize.Width * rooms[r].RoomSize.Height;

            //Disable inner walls
            for (int row = 0; row < rooms[r].RoomSize.Height; row++) {
                for (int col = 0; col < rooms[r].RoomSize.Width; col++) {
                    cellMatrix[row + rooms[r].RoomPos.Row, col + rooms[r].RoomPos.Column].North.SetActive(false);
                    cellMatrix[row + rooms[r].RoomPos.Row, col + rooms[r].RoomPos.Column].South.SetActive(false);
                    cellMatrix[row + rooms[r].RoomPos.Row, col + rooms[r].RoomPos.Column].East.SetActive(false);
                    cellMatrix[row + rooms[r].RoomPos.Row, col + rooms[r].RoomPos.Column].West.SetActive(false);
                }
            }

            //Enable outter walls
            for (int row = 0; row < rooms[r].RoomSize.Height; row++) {
                for (int col = 0; col < rooms[r].RoomSize.Width; col++) {

                    if (row + rooms[r].RoomPos.Row == rooms[r].RoomPos.Row) {
                        rooms[r].SouthWalls[col] = cellMatrix[row + rooms[r].RoomPos.Row, col + rooms[r].RoomPos.Column].South;
                        rooms[r].SouthWalls[col].SetActive(true);
                    }
                    if (row + rooms[r].RoomPos.Row == rooms[r].RoomPos.Row + rooms[r].RoomSize.Height - 1) {
                        rooms[r].NorthWalls[col] = cellMatrix[row + rooms[r].RoomPos.Row, col + rooms[r].RoomPos.Column].North;
                        rooms[r].NorthWalls[col].SetActive(true);
                    }

                    if (col + rooms[r].RoomPos.Column == rooms[r].RoomPos.Column) {
                        rooms[r].WestWalls[row] = cellMatrix[row + rooms[r].RoomPos.Row, col + rooms[r].RoomPos.Column].West;
                        rooms[r].WestWalls[row].SetActive(true);
                    }
                    if (col + rooms[r].RoomPos.Column == rooms[r].RoomPos.Column + rooms[r].RoomSize.Width - 1) {
                        rooms[r].EastWalls[row] = cellMatrix[row + rooms[r].RoomPos.Row, col + rooms[r].RoomPos.Column].East;
                        rooms[r].EastWalls[row].SetActive(true);
                    }

                    //Debug.Log("r: " + (row + rooms[r].roomPos.row) + " - c: " + (col + rooms[r].roomPos.col));
                }
            }
            //Doors
            connectDoor(r);
            connectDoor(r);
        }
    }

    /// <summary>
    /// Connects the room at roomIndex with the rest of the maze.
    /// </summary>
    /// <param name="roomIndex">Room index.</param>
    [Server]
    private void connectDoor(int roomIndex) {
        int door = Random.Range(0, 4);
        Transform t;
        GameObject spawned;
        int wall;
        switch (door) {
            default:
            case 0: //north
                wall = Random.Range(0, rooms[roomIndex].RoomSize.Width);
                t = cellMatrix[rooms[roomIndex].RoomPos.Row + rooms[roomIndex].RoomSize.Height - 1, rooms[roomIndex].RoomPos.Column + wall].North.transform;
                NetworkServer.Destroy(cellMatrix[rooms[roomIndex].RoomPos.Row + rooms[roomIndex].RoomSize.Height - 1, rooms[roomIndex].RoomPos.Column + wall].North);
                spawned = Instantiate(PrefabDoorWalls[0], t.position, t.rotation) as GameObject;
                spawned.transform.parent = transform;
                NetworkServer.Spawn(spawned);

                cellMatrix[rooms[roomIndex].RoomPos.Row + rooms[roomIndex].RoomSize.Height - 1, rooms[roomIndex].RoomPos.Column + wall].North = spawned;
                if (rooms[roomIndex].RoomPos.Row + rooms[roomIndex].RoomSize.Height < MazeSize.Row)
                    cellMatrix[rooms[roomIndex].RoomPos.Row + rooms[roomIndex].RoomSize.Height, rooms[roomIndex].RoomPos.Column + wall].South = spawned;
                break;
            case 1: //south
                wall = Random.Range(0, rooms[roomIndex].RoomSize.Width);
                t = cellMatrix[rooms[roomIndex].RoomPos.Row, rooms[roomIndex].RoomPos.Column + wall].South.transform;
                NetworkServer.Destroy(cellMatrix[rooms[roomIndex].RoomPos.Row, rooms[roomIndex].RoomPos.Column + wall].South);
                spawned = Instantiate(PrefabDoorWalls[0], t.position, t.rotation) as GameObject;
                spawned.transform.parent = transform;
                NetworkServer.Spawn(spawned);

                cellMatrix[rooms[roomIndex].RoomPos.Row, rooms[roomIndex].RoomPos.Column + wall].South = spawned;
                if (rooms[roomIndex].RoomPos.Row - 1 >= 0)
                    cellMatrix[rooms[roomIndex].RoomPos.Row - 1, rooms[roomIndex].RoomPos.Column + wall].North = spawned;
                break;
            case 2: //east
                wall = Random.Range(0, rooms[roomIndex].RoomSize.Height);
                t = cellMatrix[rooms[roomIndex].RoomPos.Row + wall, rooms[roomIndex].RoomPos.Column + rooms[roomIndex].RoomSize.Width - 1].East.transform;
                NetworkServer.Destroy(cellMatrix[rooms[roomIndex].RoomPos.Row + wall, rooms[roomIndex].RoomPos.Column + rooms[roomIndex].RoomSize.Width - 1].East);
                spawned = Instantiate(PrefabDoorWalls[0], t.position, t.rotation) as GameObject;
                spawned.transform.parent = transform;
                NetworkServer.Spawn(spawned);

                cellMatrix[rooms[roomIndex].RoomPos.Row + wall, rooms[roomIndex].RoomPos.Column + rooms[roomIndex].RoomSize.Width - 1].East = spawned;
                if (rooms[roomIndex].RoomPos.Column + rooms[roomIndex].RoomSize.Width < MazeSize.Column) //cambiar el muro de la celda de al lado que lo comparte
                    cellMatrix[rooms[roomIndex].RoomPos.Row + wall, rooms[roomIndex].RoomPos.Column + rooms[roomIndex].RoomSize.Width].West = spawned;
                break;
            case 3: //west
                wall = Random.Range(0, rooms[roomIndex].RoomSize.Height);
                t = cellMatrix[rooms[roomIndex].RoomPos.Row + wall, rooms[roomIndex].RoomPos.Column].West.transform;
                NetworkServer.Destroy(cellMatrix[rooms[roomIndex].RoomPos.Row + wall, rooms[roomIndex].RoomPos.Column].West);
                spawned = Instantiate(PrefabDoorWalls[0], t.position, t.rotation) as GameObject;
                spawned.transform.parent = transform;
                NetworkServer.Spawn(spawned);

                cellMatrix[rooms[roomIndex].RoomPos.Row + wall, rooms[roomIndex].RoomPos.Column].West = spawned;
                if (rooms[roomIndex].RoomPos.Column - 1 >= 0) //cambiar el muro de la celda de al lado que lo comparte
                    cellMatrix[rooms[roomIndex].RoomPos.Row + wall, rooms[roomIndex].RoomPos.Column - 1].East = spawned;
                break;
        }
    }

    /// <summary>
    /// Server only function.
    /// Creates the maze corridors.
    /// </summary>
    [Server]
    private void createMaze() {
        //Backtracking list (stack);
        Stack<PointRC> backTraceList = new Stack<PointRC>();

        //Controlar que todas las casillas han sido visitadas
        while (visitedCells < totalCells) {
            //Buscar una casilla random que no sea de ninguna sala
            Cell randomCell = cellMatrix[Random.Range(0, MazeSize.Row), Random.Range(0, MazeSize.Column)];
            while (randomCell.Visited) {
                randomCell = cellMatrix[Random.Range(0, MazeSize.Row), Random.Range(0, MazeSize.Column)];
            }
            //Posición de la casilla
            PointRC currPos = randomCell.Position;

            //Marcarla como visitada
            cellMatrix[currPos.Row, currPos.Column].Visited = true;
            visitedCells++;

            //añadirla a la lista de backtracking (no deberia estar este ya que entonces la primera vez se mete duplicado)
            backTraceList.Push(currPos);

            while (backTraceList.Count > 0) {
                neighbourPos = getNextNeighbour(currPos);

                //si no hay vecinos O la casilla vecina NO ha sido visitada
                if (!neighbourPos.Equals(new PointRC(-1, -1)) &&
                    !cellMatrix[neighbourPos.Row, neighbourPos.Column].Visited && cellMatrix[currPos.Row, currPos.Column].Visited) {
                    breakWall(currPos);
                    cellMatrix[neighbourPos.Row, neighbourPos.Column].Visited = true;
                    visitedCells++;
                    backTraceList.Push(currPos);
                    currPos = neighbourPos;
                }
                else {
                    currPos = backTraceList.Pop();
                    continue;
                }
            }

            //Debug.Log("Finished: " + visitedCells + "/" + totalCells);
        }
    }

    /// <summary>
    /// Gets the next neighbour where the maze will continue, and sets "wallToBreak" variable if can.
    /// </summary>
    /// <param name="point">Point of the current position.</param>
    /// <returns>Returns (-1, -1) if there is any neighbour, else returns the neighbour position, and sets "wallToBreak" variable.</returns>
    [Server]
    private PointRC getNextNeighbour(PointRC point) {
        PointRC[] neighbours = new PointRC[4]; //north, south, east, west
        int[] directions = new int[4];
        int points = 0;

        //North
        if (point.Row + 1 < MazeSize.Row) {
            if (!cellMatrix[point.Row + 1, point.Column].Visited && !cellMatrix[point.Row + 1, point.Column].IsRoom) {
                neighbours[points] = new PointRC(point.Row + 1, point.Column);
                directions[points] = 1;
                points++;
            }
        }
        //South
        if (point.Row - 1 >= 0) {
            if (!cellMatrix[point.Row - 1, point.Column].Visited && !cellMatrix[point.Row - 1, point.Column].IsRoom) {
                neighbours[points] = new PointRC(point.Row - 1, point.Column);
                directions[points] = 2;
                points++;
            }
        }
        //East
        if (point.Column + 1 < MazeSize.Column) {
            if (!cellMatrix[point.Row, point.Column + 1].Visited && !cellMatrix[point.Row, point.Column + 1].IsRoom) {
                neighbours[points] = new PointRC(point.Row, point.Column + 1);
                directions[points] = 3;
                points++;
            }
        }
        //West
        if (point.Column - 1 >= 0) {
            if (!cellMatrix[point.Row, point.Column - 1].Visited && !cellMatrix[point.Row, point.Column - 1].IsRoom) {
                neighbours[points] = new PointRC(point.Row, point.Column - 1);
                directions[points] = 4;
                points++;
            }
        }

        if (points > 0) {
            int random = Random.Range(0, points);
            wallToBreak = directions[random];
            return neighbours[random];
        }
        return new PointRC(-1, -1);
    }

    /// <summary>
    /// Breaks the wall direction set on "wallToBreak" (1: north, 2: south, 3: east, 4: west).
    /// </summary>
    /// <param name="point">Point of the current position.</param>
    [Server]
    private void breakWall(PointRC point) {
        switch (wallToBreak) {
            case 1:
                cellMatrix[point.Row, point.Column].North.SetActive(false);
                break;
            case 2:
                cellMatrix[point.Row, point.Column].South.SetActive(false);
                break;
            case 3:
                cellMatrix[point.Row, point.Column].East.SetActive(false);
                break;
            case 4:
                cellMatrix[point.Row, point.Column].West.SetActive(false);
                break;
        }
    }

    /// <summary>
    /// Draw Unity editor gizmos.
    /// </summary>
    public void OnDrawGizmos() {
        if (rooms != null) {
            for (int p = 0; p < rooms.Length; p++) {
                Gizmos.color = new Color(1, 1, 0, 0.5f);

                Vector3 cellPos = new Vector3(
                    cellMatrix[rooms[p].RoomPos.Row, rooms[p].RoomPos.Column].East.transform.position.x - (cellMatrix[rooms[p].RoomPos.Row, rooms[p].RoomPos.Column].East.transform.position.x - cellMatrix[rooms[p].RoomPos.Row, rooms[p].RoomPos.Column].West.transform.position.x) / 2,
                    cellMatrix[rooms[p].RoomPos.Row, rooms[p].RoomPos.Column].East.transform.position.y,
                    cellMatrix[rooms[p].RoomPos.Row, rooms[p].RoomPos.Column].East.transform.position.z - (cellMatrix[rooms[p].RoomPos.Row, rooms[p].RoomPos.Column].East.transform.position.z - cellMatrix[rooms[p].RoomPos.Row, rooms[p].RoomPos.Column].West.transform.position.z) / 2
                    );

                Gizmos.DrawCube(cellPos + new Vector3(wallLength * rooms[p].RoomSize.Width / 2 - wallLength / 2, -0.25f, wallLength * rooms[p].RoomSize.Height / 2 - wallLength / 2),
                    new Vector3(wallLength * rooms[p].RoomSize.Width, 0.5f, wallLength * rooms[p].RoomSize.Height));
            }
        }
    }
}
