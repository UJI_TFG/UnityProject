﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Threading;
using System;
using System.Text;
using TFGCommonLib.Networking;
using System.Collections;
using TFGCommonLib.Utils;
using TFGFran.Networking;

namespace TFG {
    class LoginForm : MonoBehaviour {

        public InputField User;
        private string username;
        public InputField Password;
        public Button LoginButton;
        public Button RegisterButton;

        public ErrorDialog ErrorDialog;

        private EventSystem eventSystem;

        private IEnumerator loginTimeOutCoroutine;
        public int loginTime = 10;

        void Awake() {
            eventSystem = EventSystem.current;

            loginTimeOutCoroutine = loginTimeOut();
        }

        public void OpenRegisterURL() {
            Application.OpenURL("http://192.168.1.10");
        }

        public void Login() {
            username = User.text;
            string pass = Password.text;

            if (username == "" || pass == "") {
                if(username == "" && pass == "")
                    ConnectionManager.Instance.AddMessage(ErrorCode.USER_PASS_EMPTY);
                else if(username == "")
                    ConnectionManager.Instance.AddMessage(ErrorCode.USER_EMPTY);
                else if(pass == "")
                    ConnectionManager.Instance.AddMessage(ErrorCode.PASS_EMPTY);
            }
            else {
                string data = username.ToLower() + ":" + pass.ToLower();
                string passCrypto = Hashing.GetHashString(data);

                try {
                    ConnectionManager.Instance.Connect();
                    ConnectionManager.Instance.Send(OpCode.LOGIN, username, passCrypto);

                    Debug.Log(">> Sent Login to server");

                    loginTimeOutCoroutine = loginTimeOut();
                    StartCoroutine(loginTimeOutCoroutine);

                    lockForm(true);
                }
                catch (Exception e) {
                    lockForm(true);
                    Debug.Log(e.StackTrace);
                    ConnectionManager.Instance.AddMessage(ErrorCode.SERVER_UNVAILABLE);
                }
            }
        }

        private IEnumerator loginTimeOut() {
            yield return new WaitForSeconds(loginTime);
            //lockForm(false);
            ConnectionManager.Instance.AddMessage(ErrorCode.LOGIN_TIMEOUT);
        }

        public void Update() {
            //Si se pulsa tab, se recorren los elementos del formulario de la misma forma que con un formulario web.
            if (Input.GetKeyDown(KeyCode.Tab)) {
                Selectable next = eventSystem.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();
                if (next != null) {
                    InputField inputfield = next.GetComponent<InputField>();

                    if (inputfield != null)
                        inputfield.OnPointerClick(new PointerEventData(eventSystem));

                    eventSystem.SetSelectedGameObject(next.gameObject, new BaseEventData(eventSystem));
                }
            }
            //Si se pulsa enter, se intenta enviar el formulario
            else if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return)) {
                LoginButton.Select();
                Login();
            }

            //Recibir mensajes
            string receivedMessage = ConnectionManager.Instance.GetReceivedMessage(OpCode.ERROR);
            if(receivedMessage != null) {
                Debug.Log(">> ERROR: " + receivedMessage);
                ConnectionManager.Instance.Disconnect();
                lockForm(false);
                StopCoroutine(loginTimeOutCoroutine);
                ErrorDialog.Show(ErrorMessageHandler.ErrorMessage(receivedMessage));
            }
            else {
                receivedMessage = ConnectionManager.Instance.GetReceivedMessage(OpCode.LOGIN_OK);
                if(receivedMessage != null) {
                    Debug.Log(">> Response: " + receivedMessage);
                    string[] profile = MessageHandler.ReadParams(receivedMessage);
                    ConnectionManager.Instance.CreatePlayerProfile(username, int.Parse(profile[0]), int.Parse(profile[1]));

                    StopCoroutine(loginTimeOutCoroutine);

                    //lockForm(false);
                    SceneManager.LoadScene("MainMenu");
                }
            }
        }

        private void lockForm(bool lockForm) {
            User.interactable = !lockForm;
            Password.interactable = !lockForm;
            LoginButton.interactable = !lockForm;
            RegisterButton.interactable = !lockForm;
        }

        public void UnlockForm() {
            lockForm(false);
        }
    }
}