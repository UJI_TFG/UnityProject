﻿using System.Collections;
using TFGCommonLib.Networking;
using UnityEngine;
using UnityEngine.UI;

public class ErrorDialog : MonoBehaviour {

    public Text ErrorText;
    private Animation animation;

    void Awake() {
        animation = GetComponent<Animation>();
    }

    private void setText(string text) {
        ErrorText.text = text;
    }

    public void Show(string text) {
        gameObject.SetActive(true);
        setText(text);
        animation.PlayQueued("FadeIn");
    }

    public void Hide() {
        animation.PlayQueued("FadeOut");
        StartCoroutine(WaitForAnimation(animation));
    }

    private IEnumerator WaitForAnimation(Animation animation) {
        do {
            yield return null;
        } while (animation.isPlaying);
        gameObject.SetActive(false);
    }
}
